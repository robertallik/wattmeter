#include <avr/io.h>	
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <string.h>
#include "i2cmaster.h"
#include "ads1115.h"
#include "KS0108.h"
#include "Font5x8.h"


#define b_SEL_X 0b00100000
#define b_ID 0b10000000
#define sizeofArray 200
#define sizeofGraph 115

void drawGraph(uint8_t max, uint8_t id, uint16_t m1);
void initBlinker();
void blink();
void initButtons();
void disableJTAG();
void disableJTAG();
void initTimer1();
void initTimer3();
void unlock();
void pollButtons();
void moveBuffer();
void drawDots(uint8_t max);

volatile uint8_t id = 0;
volatile uint8_t wMax = 5;
volatile uint8_t lock = 0;
volatile int16_t measure0 = 0;
volatile int16_t measure1 = 0;
volatile uint8_t arraySize = sizeofArray;
volatile uint8_t startBuf = 0;
volatile uint8_t stopBuf = sizeofGraph-1;
volatile float average = 0;

uint16_t ringBuffer[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


ISR(TIMER1_COMPA_vect){
	//blink();
	unlock();
	
	measure0 = ads1115_readADC_SingleEnded(ADS1115_ADDR_GND, 0, DATARATE_16SPS, FSR_6_144);
	measure1 = ads1115_readADC_SingleEnded(ADS1115_ADDR_GND, 1, DATARATE_16SPS, FSR_6_144);//M��dab pinget
	if (stopBuf == arraySize-1){
		ringBuffer[0] = (uint16_t)(measure1*0.0001875 * 3.629764 * 100); //10mV Kirjutab vastuse t�isarvuna
	} 
	else{
		ringBuffer[stopBuf+1] = (uint16_t)(measure1*0.0001875 * 3.629764 * 100);
	}
	drawGraph(wMax, id, measure1); //Joonistab graafiku
}

ISR(TIMER3_COMPA_vect){
	pollButtons(); //Vaatab, kas nuppe on vajutatud
}



void drawDots(uint8_t max){ //Joonistab punktid graafikul ja liigutab graafikut edasi
	uint8_t z = startBuf;
	uint8_t x = 13;
	uint8_t y = 0;
	float sum = 0;
	blink();
	while (z != stopBuf){
		float buf = (float)ringBuffer[z] / 100; //T�isarv ujukomaarvuks
		sum += buf;
		if (buf > (float)max){
			z++;
			x++;
			
		} else {
			y = (float)-53 / max * buf + 53;
			if (y > 53 || (buf < (0.02*max))){//
				y = 53;
			}
			GLCD_SetPixel(x, y, GLCD_Black);
			z++;
			x++;
		}
		if (z >= arraySize){
			z = 0;
		}
		
		
	}
	average = sum / sizeofGraph; //Arvutab keskmise pinge
	moveBuffer();
}

void moveBuffer(){ //Liigutab graafikut, vajadusel h�ppab algusesse
	if (startBuf == arraySize-1){
		startBuf = 0;
		stopBuf++;
	} else if(stopBuf == arraySize-1){
		stopBuf = 0;
		startBuf++;
	} else{
		startBuf++; stopBuf++;
	}
}

void drawGraph(uint8_t max, uint8_t id, uint16_t m1){
	GLCD_Clear();
	GLCD_DrawLine(12, 0, 12, 54, GLCD_Black); // Y-telg
	GLCD_DrawLine(13, 54, 127, 54, GLCD_Black); // X-telg
	
	GLCD_GotoX(0);
	GLCD_GotoLine(7);
	GLCD_PrintChar('0');
	
	GLCD_GotoX(0);
	GLCD_GotoLine(0);
	GLCD_PrintInteger(max); // Y-telje max v��rtus
	
	GLCD_GotoX(10);
	GLCD_GotoLine(7);
	GLCD_PrintString("ID:");
	GLCD_GotoX(27);
	GLCD_GotoLine(7);
	GLCD_PrintInteger(id);
	
	GLCD_GotoX(40);
	GLCD_GotoLine(7);
	GLCD_PrintString("AVG:");
	GLCD_GotoX(65);
	GLCD_GotoLine(7);
	GLCD_PrintDouble(average, 10);
	
	GLCD_GotoX(90);
	GLCD_GotoLine(7);
	GLCD_PrintDouble((double)m1*0.0001875 * 3.629764, 1000);
	GLCD_GotoX(120);
	GLCD_GotoLine(7);
	GLCD_PrintChar('V');
	
	drawDots(max);
	GLCD_Render();
}

void initBlinker(){ //Debug LED
	DDRE |= 0b0100000;
	PORTE |= 0b01000000;
}

void blink(){ //Debug LED
	PORTE ^= 0b01000000;
}

void initButtons(){ // Nuppude pinnid sisendiks
	DDRD &= ~((1<<5) | (1<<7)); //input
}

void disableJTAG(){ // V�tab JTAG maha, sest LCD kasutab PORTF-i
	MCUCR |= (1<<JTD);
	MCUCR |= (1<<JTD);
}
void initTimer1(){ // Timer, mis uuendab ekraani ja teeb m��tmised
	TCCR1B = (1<<WGM12) | (1<<CS12); //ctc /256 prescaler
	OCR1A = 0x1850; //10Hz
	TIMSK1 = (1<<OCIE1A); //interrupt en, match A
}
void initTimer3(){ //Timer, mis kontrollib nuppe
	TCCR3B = (1<<WGM32) | (1<<CS32); //ctc /256 prescaler
	OCR3A = 0x0138; //5ms
	TIMSK3 = (1<<OCIE3A); //interrupt en, match A
}
void unlock(){ // Debounce funktsioon: Kui nuppu on vajutatud siis see l�heb lukku kuni timer1 selle lahti teeb
	if ((PIND & (1<<5))){
		lock ^= ~(1<<5);
	} 
	if ((PIND & (1<<7))){
		lock ^= ~(1<<7);
	}
}

void pollButtons(){ // Kontrollib, kas nuppe on vajutatud ja lukustab need
	if (!(PIND & b_ID) && !(lock & b_ID)){
		lock |= b_ID;
		id++;
		blink();
	}
	if (!(PIND & b_SEL_X) && !(lock & b_SEL_X)){
		lock |= b_SEL_X;
		if (wMax != 20){
			wMax += 5;
		} else {
			wMax = 5;
		}
		blink();
	}
}

int main(void){
	disableJTAG();
	//UINT bw;
	
	GLCD_Setup();
	GLCD_Clear();
	GLCD_SetFont(Font5x8, 5, 8, GLCD_Merge);
	GLCD_Render();
	initBlinker();
	initButtons();
	
	initTimer1();
	initTimer3();
	
	i2c_init();
	
	
	sei(); // Lubab interrupte
	while (1){
	}
	
		
	return 0;
}