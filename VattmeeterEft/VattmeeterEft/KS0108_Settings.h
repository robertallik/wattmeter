﻿#ifndef KS0108_SETTINGS_H_INCLUDED
#define KS0108_SETTINGS_H_INCLUDED
/*
||
||  Filename:	 		KS0108_Settings.h
||  Title: 			    KS0108 Driver Settings
||  Author: 			Efthymios Koktsidis
||	Email:				efthymios.ks@gmail.com
||  Compiler:		 	AVR-GCC
||	Description:
||	Settings for the KS0108 driver. 
||
*/

//----- Configuration -------------//
//Chip Enable Pin
#define GLCD_Active_Low		0

//GLCD pins					PORT, PIN
#define GLCD_D0				C, 6
#define GLCD_D1				C, 7
#define GLCD_D2				F, 7
#define GLCD_D3				F, 6
#define GLCD_D4				F, 5
#define GLCD_D5				F, 4
#define GLCD_D6				F, 1
#define GLCD_D7				F, 0

#define GLCD_DI				B, 7
#define GLCD_RW				B, 4
#define GLCD_EN				D, 4
#define GLCD_CS1			B, 6
#define GLCD_CS2			B, 5
#define GLCD_RST			D, 6
//---------------------------------//
#endif